<?php 
require_once("fight.php");

class Harimau extends Hewan {
    public $jumlahKaki = 4;
    public $keahlian = "Lari Cepat";

    use Fight;

    public function serangan() {
        return $this->attackPower = 7;
    }

    public function pertahanan() {
        return $this->defensePower = 8;
    }
}

?>