<?php 
require_once("fight.php");

class Elang extends Hewan {
    public $jumlahKaki = 2;
    public $keahlian = "Terbang Tinggi";

    use Fight;

    public function serangan() {
        return $this->attackPower = 10;
    }

    public function pertahanan() {
        return $this->defensePower = 5;
    }
}



?>