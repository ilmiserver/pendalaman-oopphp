<?php 
    require_once("hewan.php");
    require_once("Elang.php");
    require_once("harimau.php");
    require_once("fight.php");

    $elang = New Elang("Elang");
    echo "Nama: $elang->name <br>";
    echo "Darah: $elang->hp <br>";
    echo "Jumlah Kaki: $elang->jumlahKaki <br>";
    echo "Keahlian: $elang->keahlian <br>";
    echo "Attack Power:" . $elang->serangan() ."<br>";
    echo "Defense Power:" . $elang->pertahanan() ."<br><br>";

    $macan =  New Harimau("Harimau");
    echo "Nama: $macan->name <br>";
    echo "Darah: $macan->hp <br>";
    echo "Jumlah Kaki: $macan->jumlahKaki <br>";
    echo "Keahlian: $macan->keahlian <br>";
    echo "Attack Power:" . $macan->serangan() ."<br>";
    echo "Defense Power:" . $macan->pertahanan() ."<br><br>";